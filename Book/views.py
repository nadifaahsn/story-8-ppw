from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
import requests

def index(request):
    return render(request, 'Book/book.html')

def getBooks(request):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET['keyword'])
    data = response.json()
    return JsonResponse(data)
