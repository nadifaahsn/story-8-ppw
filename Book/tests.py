from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from datetime import date
from .views import getBooks, index
from .models import Book
import json
import requests

class TestBook(TestCase):
    def test_url_book(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template_book(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Book/book.html')

    def test_check_landing_page_html(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('<input type="text"', content)
        self.assertIn('<button', content)
        self.assertIn('<th>Cover</th>', content)
        self.assertIn('<th>Title</th>', content)
        self.assertIn('<th>Author</th>', content)
        self.assertIn('<th>Publish Date</th>', content)
        self.assertIn('<th>Publisher</th>', content)

    def test_models_create_book(self):
        Book.objects.create(
            id= '1',
            cover = 'https://p.bigstockphoto.com/GeFvQkBbSLaMdpKXF1Zv_bigstock-Aerial-View-Of-Blue-Lakes-And--227291596.jpg',
            title = 'IPA',
            author = 'Hasna',
            publishedDate = date(2020, 11, 11),
            publisher = 'Gramedia'
        )
        total = Book.objects.all().count()
        self.assertEqual(total,1)

    def test_views_story8(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_fungsi_data(self):
        response = Client().get('/getBooks?keyword=doctor')
        self.assertEqual(response.status_code,200)

