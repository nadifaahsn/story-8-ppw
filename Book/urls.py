from django.urls import path
from . import views

app_name = 'Book'

urlpatterns = [
    path('', views.index, name='index'),
    path('getBooks', views.getBooks, name='getBooks'),
]

